#include "KelvinUnit.h"

static void toCelsius(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Celsius());
}

static void toFahrenheit(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Fahrenheit());
}

TemperatureUnit* convertTo_Kelvin(void) {
	static TemperatureUnit* kelvinUnit;
	static int initialized = 0;

	if (0 == initialized) {
		defaultImplementation(&kelvinUnit);
		kelvinUnit->celsius = toCelsius;
		kelvinUnit->fahrenheit = toFahrenheit;
		initialized = 1;
	}

	return &kelvinUnit;
}
