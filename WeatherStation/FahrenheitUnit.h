#ifndef FAHRENHEIT_H
#define FAHRENHEIT_H

#include "TemperatureUnit.h"
#include "Temperature.h"

/*Possible Conversions*/
#include "CelsiusUnit.h"
#include "KelvinUnit.h"

TemperatureUnit* convertTo_Fahrenheit(void);

#endif
