#ifndef KNOTS_H
#define KNOTS_H

#include "Velocity.h"
#include "VelocityUnit.h"

/*Possible Conversions*/
#include "MilesPerHour.h"

VelocityUnit* convertTo_Knots(void);

#endif
