#ifndef VELOCITY_H
#define VELOCITY_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "VelocityUnit.h"

/* Velocity.h	Definitions for performing operations on speed

Typical use:

#include "Velocity.h"

*/

/* The Velocity type itself */
typedef struct _Velocity Velocity;

struct _Velocity {
	VelocityUnit* unit;
	int speed;
};

/**** Operations ****/

extern Velocity* Velocity_createVelocity(void);
/* Synopsis: (create a new Velocity) */

extern void Velocity_destroyVelocity(Velocity* instance);
/* Synopsis: (deallocate "instance") */

extern void Velocity_changeUnit(Velocity* instance, VelocityUnit* newUnit);
/* Synopsis: (convert the Velocity "instance"
to have a unit of "newUnit")
*/

extern int Velocity_getSpeed(Velocity* instance);
/* Synopsis: (fetch the "speed" value
associated with the "instance")
*/

extern void Velocity_setSpeed(Velocity* instance, int newSpeed);
/* Synopsis: (set the "speed" value
associated with the "instance"
to "newSpeed")
*/

extern char *Velocity_getUnitName(Velocity* instance);
/* Synopsis: (fetch the "name" value
associated with the "instance")
*/

extern void Velocity_setUnitName(Velocity* instance, char *name);
/* Synopsis: (set the "name" value
associated with the "instance"
to "name")
*/

#endif
