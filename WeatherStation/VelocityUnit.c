#include "VelocityUnit.h"

static void defaultConversion_Knots(VelocityUnit* unit) {
	/*code path will reach here if the knots conversion is not supported in the concrete class*/
	//logUnsupportedConversion("to knots", unit->name);
}

static void defaultConversion_MPH(VelocityUnit* unit) {
	/*code path will reach here if the mph conversion is not supported in the concrete class*/
	//logUnsupportedConversion("to MPH", unit->name);

}


void Velocity_defaultImplementation(VelocityUnit* unit)
{
	unit->mph = defaultConversion_MPH;
	unit->knots = defaultConversion_Knots;

}
