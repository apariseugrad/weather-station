#ifndef WIND_H
#define WIND_H
	
#include <stdlib.h>

#include "Direction.h"
#include "Velocity.h"



/* Wind.h	Definitions for performing operations on Wind

Typical use:

#include "Wind.h"



*/

/* The Wind type itself */
typedef struct _Wind Wind;

struct _Wind {
	char* directionName;
	int direction;
	Velocity* velocity;
};


/**** Operations ****/

extern Wind* Wind_construct(void);
/* Synopsis: (create a new Wind) */

extern void Wind_destroy(Wind* instance);
/* Synopsis: (deallocate "instance") */

extern void Wind_changeDirection(Wind* instance, int newDirection);
/* Synopsis: (convert the wind "instance"
				to have a direction of "newDirection")
*/

extern Velocity* Wind_getVelocity(Wind* instance);
/* Synopsis: (fetch the "Velocity" value
		associated with the "instance")
*/

extern void Wind_setVelocity(Wind* instance, Velocity* newVelocity);
/* Synopsis: (set the "Velocity" value
		associated with the "instance"
		to "newVelocity")
*/


#endif
