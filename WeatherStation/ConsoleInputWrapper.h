#include "ConsoleInput.h"

#ifndef CONSOLEINPUTWRAPPER_H
#define CONSOLEINPUTWRAPPER_H

typedef struct ConsoleInput_Wrapper
{
	void					*data;
	ConsoleInput_Operations	*operations;
} ConsoleInput_Wrapper;

typedef ConsoleInput_Wrapper(*create_handle)	(void);
typedef void(*destroy_handle)	(ConsoleInput_Wrapper **ptr);

typedef struct factory_ConsoleInput
{
	create_handle  create;
	destroy_handle destroy;
} factory_ConsoleInput;

#endif
