/* Andrew Parise ~ 14 September 2017 - 24 September 2017 */
#undef main

#include "stdafx.h"
#include "conio.h"
#include <iostream> 
#include <string> 
#include <stdio.h>
#include <stdlib.h>
#include <sstream> 

extern "C" {
	#include "DisplayWeather.h"
}
int displayMenu();
Weather* enterReading(Weather* weatherReading);
void displayReading(Weather* weatherReading);

int isInputInteger(char* consoleInput, int digits = 1) {
	//Returns 1 if it is an integer, 0 if it is not
	if (digits == 1) {
		if (isdigit(consoleInput[0])) {
			return 1;
		}
	}
	int i = 0;

	while (consoleInput[i] != '\0' || consoleInput[i] != '\n') {
		if (!isdigit(consoleInput[i])) {
			return 0;
		}
		i++;
	}

	return 1;
}

char* readConsoleInput() {
	void *ptr;
	ptr = calloc(32, sizeof(char));
	fgets((char*)ptr, 10, stdin);
	return (char*) ptr;
}


int displayMenu() {

	Weather* weatherReading = Weather_construct();
	Wind* defaultWind = Wind_construct();
	Temperature* defaultTemperature = Temperature_construct();

	free(weatherReading->temperature);
	weatherReading->temperature = defaultTemperature;

	free(weatherReading->wind);
	weatherReading->wind = defaultWind;

	while (1) {

		printf("*********************************************\n");
		printf("** Weather Station Menu **\n");
		printf("*********************************************\n");
		printf("(1) Display Weather Reading\n(2) Input New Reading\n(3) Exit\n");
		printf("Please enter the number corresponding to your selection, then press enter.\n");
		printf("> ");

		char* userInput = readConsoleInput();
		if (isInputInteger(userInput) == 0) {
			printf("Please enter an integer.\n");
			continue;
		}
		
		int userInputInt = 0;

		std::stringstream(userInput) >> userInputInt;
		

		switch (userInputInt) {
			case 1:
				displayReading(weatherReading);
				break;
			case 2:
				weatherReading = enterReading(weatherReading);
				break;
			case 3:
				return 0;
			default:
				printf("Error: Invalid input. System expect either '1', '2', or '3'.\n");
				break;
			}
	}
	return 0;

}

Weather * enterReading(Weather * weatherReading)
{
	if (weatherReading == NULL) {
		weatherReading = Weather_construct();
	}

	char* weathersysName = (char*)malloc(sizeof(char));
	std::cout << "Please enter the name of the weather station: " << std::endl;

	std::cout << "> ";
	weathersysName = readConsoleInput();
	std::cout << std::endl;

	weatherReading->stationName = weathersysName;

	int curTemperature = 0;
	std::cout << "Please enter the current temperature: " << std::endl;
	std::cout << "> ";
	std::cin >> curTemperature;
	if (weatherReading->temperature->degrees == NULL) {
		weatherReading->temperature->degrees = (int)malloc(sizeof(int));
	}
	weatherReading->temperature->degrees = curTemperature;

	int curWindSpeed = 0;
	std::cout << "Please enter the speed of the wind: " << std::endl;
	std::cout << "> ";
	std::cin >> curWindSpeed;
	std::cout << std::endl;
	if (weatherReading->wind->velocity->speed == NULL) {
		weatherReading->wind->velocity->speed = (int)malloc(sizeof(int));
	}
	weatherReading->wind->velocity->speed = curWindSpeed;

	char* weatherDirName = (char*)malloc(sizeof(char));
	std::cout << "Please enter the direction of the wind: \n";
	std::cout << "> ";
	std::cin.getline(weatherDirName, 32);
	if (weatherReading->wind->directionName == NULL) {
		weatherReading->wind->directionName = (char*)malloc(sizeof(char));
	}

	weatherReading->wind->directionName = weatherDirName;
	getchar();
	return weatherReading;
}

void displayReading(Weather * weatherReading)
{
	std::cout << std::endl;
	std::cout << "The " << weatherReading->stationName << " Weather Station:" << std::endl;
	std::cout << std::endl;
	std::cout << "The Current Temperature is " << weatherReading->temperature->degrees << " degrees Fahrenheit." << std::endl;
	std::cout << "The Current Wind Speed is " << weatherReading->wind->velocity->speed << " " << weatherReading->wind->directionName << std::endl;
	std::cout << std::endl;
	std::cout << "Press any button to continue..." << std::endl;

	_getch();
}



int main(int argc, char** argv)
{
	std::cout << "************************************************************" << std::endl;
	std::cout << "* Weather Station Application by Andrew Parise - Fall 2017 *" << std::endl;
	std::cout << "************************************************************\n" << std::endl;
	
	displayMenu();

	return 0;
}
