#include "Wind.h"

Wind* Wind_construct(void)
{
	Wind* instance = (Wind*)malloc(sizeof(Wind));

	if (NULL != instance) {
		//set initial state

		char* directionInst = (char*)malloc(sizeof(char));

		instance->directionName = directionInst;

		Velocity* veloctyInstance = (Velocity*)malloc(sizeof(Velocity));
		veloctyInstance = Velocity_createVelocity();

		instance->velocity = veloctyInstance;
		instance->direction = North;

	}
	return instance;
}

void Wind_destroy(Wind* instance)
{
	instance->direction = 0;
	Velocity_destroyVelocity(instance->velocity);
	free(instance);
}


void Wind_changeDirection(Wind* instance, int newDirection)
{
	free(instance->direction);
	instance->direction = newDirection;
}


Velocity* Wind_getVelocity(Wind* instance)
{
	return instance->velocity;
}

void Wind_setVelocity(Wind* instance, Velocity* newVelocity)
{
	free(instance->velocity);
	instance->velocity = newVelocity;
}
