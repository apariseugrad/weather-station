#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include "TemperatureUnit.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/* Temperature.h	Definitions for performing operations on temperatures

	Typical use:
		
		#include "Temperature.h"



*/

/* The Temperature type itself */
typedef struct _Temperature Temperature;

struct _Temperature {
	TemperatureUnit* unit;
	int degrees;
};


/**** Operations ****/

extern Temperature* Temperature_construct(void);
/* Synopsis: (create a new temperature) */

extern void Temperature_destroy(Temperature* instance);
/* Synopsis: (deallocate "instance") */

extern void Temperature_changeUnit(Temperature* instance, TemperatureUnit* newUnit);
/* Synopsis: (convert the temperature "instance" 
				to have a unit of "newUnit") 
*/

extern int Temperature_getDegrees(Temperature* instance);
/* Synopsis: (fetch the "degrees" value
				associated with the "instance")
*/

extern void Temperature_setDegrees(Temperature* instance, int newDegrees);
/* Synopsis: (set the "degrees" value
				associated with the "instance"
				to "newDegrees")
*/

extern char *Temperature_getUnitName(Temperature* instance);
/* Synopsis: (fetch the "name" value
				associated with the "instance")
*/

extern void Temperature_setUnitName(Temperature* instance, char *name);
/* Synopsis: (set the "name" value
				associated with the "instance"
				to "newDegrees")
*/

#endif
