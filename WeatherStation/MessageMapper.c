#include "MessageMapper.h"
#include <stdlib.h>

char* MessageMapper__OUTPUT_TEMPERATURE(int degrees, const char* unitName) {
	const char* msgTemplate = Message_getMessage(OUTPUT_TEMPERATURE);
	char* buffer = calloc(1024, sizeof(char));
	snprintf(buffer, sizeof(buffer), msgTemplate, degrees, unitName);
	return buffer;
}

char* MessageMapper__OUTPUT_WIND(int speed, char const * direction) {
	const char* msgTemplate = Message_getMessage(OUTPUT_WIND);
	char* buffer = calloc(1024, sizeof(char));
	snprintf(buffer, sizeof(buffer), msgTemplate, speed, direction);
	return buffer;
}
