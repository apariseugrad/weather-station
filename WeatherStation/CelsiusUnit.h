#ifndef CELSIUS_H
#define CELSIUS_H

#include "TemperatureUnit.h"
#include "Temperature.h"

/*Possible Conversions*/
#include "FahrenheitUnit.h"
#include "KelvinUnit.h"

TemperatureUnit* convertTo_Celsius(void);

#endif
