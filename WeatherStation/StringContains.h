#ifndef STRINGCONTAINS_H
#define STRINGCONTAINS_H

#include <stdio.h>
#include <string.h>

extern int String_contains(char character, char* str);
/* Synopsis: (checks if the "character"
				can be found in "str".
				Returns "0" if False
				Returns "1" if True)
*/

#endif // !STRINGCONTAINS_H

