#include "StringContains.h"

int String_contains(char character, char *str)
{
	if (strchr(str, character))
	{
		return 1;
	}

	return 0;
}

