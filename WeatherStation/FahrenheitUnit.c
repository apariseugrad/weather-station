#include "FahrenheitUnit.h"

static void toCelsius(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Celsius());
}

static void toKelvin(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Kelvin());
}

TemperatureUnit* convertTo_Fahrenheit(void) {
	static TemperatureUnit* fahrenheitUnit;
	static int initialized = 0;

	if (0 == initialized) {
		defaultImplementation(&fahrenheitUnit);
		fahrenheitUnit->celsius = toCelsius;
		fahrenheitUnit->kelvin = toKelvin;

		initialized = 1;
	}

	return &fahrenheitUnit;
}