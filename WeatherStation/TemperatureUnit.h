#ifndef TEMPERATUREUNIT_H
#define TEMPERATUREUNIT_H

/* The TemperatureUnit type itself */
typedef struct _TemperatureUnit TemperatureUnit;

typedef void(*UnitChangeFunc_Fahrenheit)(TemperatureUnit*);
typedef void(*UnitChangeFunc_Celsius)	(TemperatureUnit*);
typedef void(*UnitChangeFunc_Kelvin)	(TemperatureUnit*);

struct _TemperatureUnit {
	char* name;
	UnitChangeFunc_Fahrenheit fahrenheit;
	UnitChangeFunc_Celsius celsius;
	UnitChangeFunc_Kelvin kelvin;
};


//allows for flexible pointer error handling
void defaultImplementation(TemperatureUnit* unit);

#endif
