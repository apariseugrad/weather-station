
#include "Velocity.h"

Velocity* Velocity_createVelocity(void) {
	Velocity* instance = (Velocity*)malloc(sizeof(Velocity));

	if (NULL != instance) {
		//set initial state
		VelocityUnit* unitInstance = (VelocityUnit*)malloc(sizeof(VelocityUnit));
		unitInstance->mph;

		char *unitName = (char*)malloc(sizeof(char));
		unitName = "Mile(s) Per Hour";
		instance->unit = unitInstance;
		instance->unit->name = unitName;

		//init other attributes
		int intDefault = (int)malloc(sizeof(int));
		intDefault = 0;
		instance->speed = intDefault;
	}
	return instance;
}

void Velocity_destroyVelocity(Velocity* instance) {
	instance->speed = 0;
	free(instance);
}

void Velocity_changeUnit(Velocity* instance, VelocityUnit* newUnit) {
	instance->unit = newUnit;
}

int Velocity_getSpeed(Velocity* instance) {
	return instance->speed;
}

void Velocity_setSpeed(Velocity* instance, int newSpeed) {
	instance->speed = newSpeed;
}

char *Velocity_getUnitName(Velocity* instance) {
	return instance->unit->name;
}

void Velocity_setUnitName(Velocity* instance, char *name) {
	instance->unit->name = name;
}
