#ifndef MESSAGEMAPPER_H
#define MESSAGEMAPPER_H

#include <string.h>
#include <stdio.h>

#include "Message.h"

extern char* MessageMapper__OUTPUT_TEMPERATURE(int degrees, const char* unitName);

extern char* MessageMapper__OUTPUT_WIND(int speed, const char* direction);


#endif // !MESSAGEMAPPER_H
