#include <string.h>
#include <stdlib.h>

#include "MessageFactory.h"
#include "MessageMapper.h"
#include "Message.h"
#include "Wind.h"
#include "Velocity.h"
#include "VelocityUnit.h"
#include "Temperature.h"
#include "TemperatureUnit.h"
#include "AbstractMessage.h"
#include "InputMessage.h"
#include "OutputMessage.h"

static char*	MessageFactory_getWelcome					();
static char*	MessageFactory_getPressAnyKey				();
static char*	MessageFactory_getInputTemperatureDegrees	();
static char*	MessageFactory_getInputTemperatureUnitName	();
static char*	MessageFactory_getInputWindDirection		();
static char*	MessageFactory_getInputWindVelocity			();
static char*	MessageFactory_getOutputTemperature			(Temperature* tempInstance);
static char*	MessageFactory_getOutputWind				(Wind* windInstance);

MessageFactory* MessageFactory_construct(void) {
	
	MessageFactory* instance = (MessageFactory*)malloc(sizeof(MessageFactory));

	if (instance != NULL) {
		instance->getInputTemperatureDegrees = MessageFactory_getInputTemperatureDegrees;
		instance->getInputTemperatureUnitName = MessageFactory_getInputTemperatureUnitName;
		instance->getInputWindDirection = MessageFactory_getInputWindDirection;
		instance->getInputWindVelocity = MessageFactory_getInputWindVelocity;
		instance->getOutputTemperature = MessageFactory_getOutputTemperature;
		instance->getOutputWind = MessageFactory_getOutputWind;
		instance->getPressAnyKey = MessageFactory_getPressAnyKey;
		instance->getWelcome = &MessageFactory_getWelcome;
	}

	return instance;
}

void MessageFactory_destruct(MessageFactory* instance)
{
	free(instance);
}

char* MessageFactory_getWelcome() {

	//OutputMessage* message = OutputMessage_construct(malloc(sizeof(OutputMessage)));
	
	//message->setContent(&message->abstractMessage, _strdup(Message_getMessage(WELCOME)));
	
	return _strdup(Message_getMessage(WELCOME));
}

char* MessageFactory_getPressAnyKey()
{
	OutputMessage* message = (OutputMessage*)malloc(sizeof(OutputMessage));
	OutputMessage_construct(&message);
	message->setContent(&message->abstractMessage, _strdup(Message_getMessage(PRESS_ANY_KEY)));
	return message;
}

char* MessageFactory_getInputTemperatureDegrees()
{
	//OutputMessage* message = (OutputMessage*)malloc(sizeof(OutputMessage));
	//OutputMessage_construct(&message);
	//message->setContent(&message->abstractMessage, _strdup(Message_getMessage(INPUT_TEMPERATURE_VALUE)));
	return _strdup(Message_getMessage(INPUT_TEMPERATURE_VALUE));
}

char* MessageFactory_getInputTemperatureUnitName()
{
	//OutputMessage* message = (OutputMessage*)malloc(sizeof(OutputMessage));
	//OutputMessage_construct(&message);
	//message->setContent(&message->abstractMessage, _strdup(Message_getMessage(INPUT_TEMPERATURE_UNIT)));
	return _strdup(Message_getMessage(INPUT_TEMPERATURE_UNIT));
}

char* MessageFactory_getInputWindDirection()
{
	//OutputMessage* message = (OutputMessage*)malloc(sizeof(OutputMessage));
	//OutputMessage_construct(&message);
	//message->setContent(&message->abstractMessage, _strdup(Message_getMessage(INPUT_WIND_DIRECTION)));
	return _strdup(Message_getMessage(INPUT_WIND_DIRECTION));
}

char* MessageFactory_getInputWindVelocity()
{
	//OutputMessage* message = (OutputMessage*)malloc(sizeof(OutputMessage));
	//OutputMessage_construct(&message);
	//message->setContent(&message->abstractMessage, _strdup(Message_getMessage(INPUT_WIND_VELOCITY)));
	return _strdup(Message_getMessage(INPUT_WIND_VELOCITY));
}

char* MessageFactory_getOutputTemperature(Temperature* tempInstance)
{
	//InputMessage* message = InputMessage_construct();
	//message->setContent(&message->abstractMessage, _strdup(MessageMapper__OUTPUT_TEMPERATURE(tempInstance->degrees, tempInstance->unit->name)));
	return _strdup(MessageMapper__OUTPUT_TEMPERATURE(tempInstance->degrees, tempInstance->unit->name));
}

char* MessageFactory_getOutputWind(Wind* windInstance)
{
	//InputMessage* message = InputMessage_construct();
	//message->setContent(&message->abstractMessage, _strdup(MessageMapper__OUTPUT_WIND(windInstance->velocity->speed, DIRECTION_STRING[windInstance->direction])));
	return _strdup(MessageMapper__OUTPUT_WIND(windInstance->velocity->speed, DIRECTION_STRING[windInstance->direction]));
}
