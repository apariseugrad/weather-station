#ifndef VELOCITYUNIT_H
#define VELOCITYUNIT_H

/* The VelocityUnit type itself */
typedef struct _VelocityUnit VelocityUnit;

typedef void(*UnitChangeFunc_MPH)(VelocityUnit*);
typedef void(*UnitChangeFunc_Knots)(VelocityUnit*);

struct _VelocityUnit {
	char *name;
	UnitChangeFunc_MPH mph;
	UnitChangeFunc_Knots knots;
};


//allows for flexible pointer error handling
void Velocity_defaultImplementation(VelocityUnit* unit);

#endif
