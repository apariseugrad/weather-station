#include "Knots.h"

static void toMPH(Velocity* context) {
	Velocity_changeUnit(context, convertTo_MPH());
}

VelocityUnit* convertTo_Knots(void) {
	static VelocityUnit* knotsUnit;
	static int initialized = 0;

	if (0 == initialized) {
		Velocity_defaultImplementation(&knotsUnit);
		knotsUnit->mph = toMPH;

		initialized = 1;
	}

	return &knotsUnit;
}
