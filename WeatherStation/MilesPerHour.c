#include "MilesPerHour.h"

static void toKnots(Velocity* context) {
	Velocity_changeUnit(context, convertTo_Knots());
}

VelocityUnit* convertTo_MPH(void) {
	static VelocityUnit* mphUnit;
	static int initialized = 0;

	if (0 == initialized) {
		Velocity_defaultImplementation(&mphUnit);
		mphUnit->knots = toKnots;
		char* unitName = (char*)malloc(sizeof(char));
		unitName = "Miles Per Hour";
		initialized = 1;
	}

	return &mphUnit;
}
