#include "Temperature.h"


Temperature* Temperature_construct(void) {
	Temperature* instance = (Temperature*) malloc(sizeof (Temperature));

	if (NULL != instance) {
		//set initial state
		TemperatureUnit* unitInstance = (TemperatureUnit*)malloc(sizeof(TemperatureUnit));
		unitInstance->fahrenheit;
		
		char *unitName = (char*)malloc(sizeof(char));
		unitName = "Fahrenheit";
		instance->unit  = unitInstance;
		instance->unit->name = unitName;

		//init other attributes
		int intDefault = (int)malloc(sizeof(int));
		intDefault = 0;
		instance->degrees = intDefault;
	}
	return instance;
}

void Temperature_destroy(Temperature* instance) {
	instance->degrees	= 0;
	free(instance);
}

void Temperature_changeUnit(Temperature* instance, TemperatureUnit* newUnit) {
	instance->unit = newUnit;
}

int Temperature_getDegrees(Temperature* instance) {
	return instance->degrees;
}

void Temperature_setDegrees(Temperature* instance, int newDegrees) {
	instance->degrees = newDegrees;
}

char *Temperature_getUnitName(Temperature* instance) {
	return instance->unit->name;
}

void Temperature_setUnitName(Temperature* instance, char *name) {
	instance->unit->name = name;
}



