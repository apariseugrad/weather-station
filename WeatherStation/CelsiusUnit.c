#include "CelsiusUnit.h"

static void toFahrenheit(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Fahrenheit());
}

static void toKelvin(Temperature* context) {
	Temperature_changeUnit(context, convertTo_Kelvin());
}

TemperatureUnit* convertTo_Celsius(void) {
	static TemperatureUnit* celsiusUnit;
	static int initialized = 0;

	if (0 == initialized) {
		defaultImplementation(&celsiusUnit);
		celsiusUnit->fahrenheit = toFahrenheit;
		celsiusUnit->kelvin = toKelvin;

		initialized = 1;
	}

	return &celsiusUnit;
}