#include "TemperatureUnit.h"

static void defaultConversion_Fahrenheit(TemperatureUnit* unit) {
	/*code path will reach here if the fahrenheit conversion is not supported in the concrete class*/
	//logUnsupportedConversion("to Fahrenheit", unit->name);
}

static void defaultConversion_Celsius(TemperatureUnit* unit) {
	/*code path will reach here if the celsius conversion is not supported in the concrete class*/
	//logUnsupportedConversion("to Celsius", unit->name);

}

static void defaultConversion_Kelvin(TemperatureUnit* unit) {
	/*code path will reach here if the kelvin conversion is not supported in the concrete class*/
	//logUnsupportedConversion("to Kelvin", unit->name);

}

void defaultImplementation(TemperatureUnit* unit)
{
	unit->celsius = defaultConversion_Celsius;
	unit->fahrenheit = defaultConversion_Fahrenheit;
	unit->kelvin = defaultConversion_Kelvin;

}
