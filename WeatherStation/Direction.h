#ifndef DIRECTION_H
#define DIRECTION_H


#define FOREACH_DIRECTION(DIRECTION) \
        DIRECTION(North)  \
        DIRECTION(North_East)   \
        DIRECTION(East)   \
        DIRECTION(South_East)  \
		DIRECTION(South)  \
		DIRECTION(South_West)  \
		DIRECTION(West)  \
		DIRECTION(North_West)  \

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum DIRECTION_ENUM {
	FOREACH_DIRECTION(GENERATE_ENUM)
};

static const char *DIRECTION_STRING[] = {
	FOREACH_DIRECTION(GENERATE_STRING)
};

#endif
