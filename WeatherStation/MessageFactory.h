#ifndef MESSAGEFACTORY_H
#define MESSAGEFACTORY_H

#include "MessageFactory.h"
#include "MessageMapper.h"
#include "Message.h"
#include "Wind.h"
#include "Velocity.h"
#include "VelocityUnit.h"
#include "Temperature.h"
#include "TemperatureUnit.h"
#include "AbstractMessage.h"
#include "InputMessage.h"
#include "OutputMessage.h"

typedef struct _MessageFactory MessageFactory;

struct _MessageFactory {
	char*	(*getWelcome)					();
	char*	(*getPressAnyKey)				();
	char*	(*getInputWindDirection)		();
	char*	(*getInputWindVelocity)			();
	char*	(*getInputTemperatureUnitName)	();
	char*	(*getInputTemperatureDegrees)	();
	char*	(*getOutputTemperature)			(Temperature* tempInstance);
	char*	(*getOutputWind)				(Wind* windInstance);
} _MessageFactory;

extern MessageFactory* MessageFactory_construct(void);
extern void MessageFactory_destruct(MessageFactory* instance);

#endif // !MESSAGEFACTORY_H
