#ifndef MILESPERHOUR_H
#define MILESPERHOUR_H

#include "Velocity.h"
#include "VelocityUnit.h"

/*Possible Conversions*/
#include "Knots.h"

VelocityUnit* convertTo_MPH(void);

#endif
