#ifndef KELVIN_H
#define KELVIN_H

#include "Temperature.h"
#include "TemperatureUnit.h"

/*Possible Conversions*/
#include "CelsiusUnit.h"
#include "FahrenheitUnit.h"

TemperatureUnit* convertTo_Kelvin(void);

#endif
